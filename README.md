# Twitter Disaster prediction.

https://www.kaggle.com/c/nlp-getting-started

Twitter has become an important communication channel in times of emergency.
The ubiquitousness of smartphones enables people to announce an emergency they’re observing in real-time. Because of this, more agencies are interested in programatically monitoring Twitter (i.e. disaster relief organizations and news agencies).

But, it’s not always clear whether a person’s words are actually announcing a disaster. Take this example:





The author explicitly uses the word “ABLAZE” but means it metaphorically. This is clear to a human right away, especially with the visual aid. But it’s less clear to a machine.

In this competition, you’re challenged to build a machine learning model that predicts which Tweets are about real disasters and which one’s aren’t. You’ll have access to a dataset of 10,000 tweets that were hand classified. If this is your first time working on an NLP problem, we've created a quick tutorial to get you up and running.

# APPROACH

1. Preprocess data
    * Using Weight of Evidence(WOE)
    * TF-IDF Vectorizer
2. Models
    * Ridge Classifier
    * SVM
    * Decision Tree
    * XGBoost
    * Ensemble of above methods

